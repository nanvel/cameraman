#!/bin/sh

DEVICE=/dev/video0
PHOTOS_DIR=/home/cameraman/photos

[ -c $DEVICE ] && fswebcam --device $DEVICE --no-banner --png --save ${PHOTOS_DIR}/photo_`eval date +%Y_%m_%d_%H_%M_%s`.png
