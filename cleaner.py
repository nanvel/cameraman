#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import datetime

from settings import PHOTOS_DIR, REMOVE_AFTER_HOURS


def remove_old_photos():
    print 'Removing old files ...'
    min_time = datetime.datetime.now() - datetime.timedelta(hours=REMOVE_AFTER_HOURS)
    for root, dirnames, filenames in os.walk(PHOTOS_DIR):
        print root, dirnames, filenames
        for filename in filenames:
            try:
                parts = filename.split('_')
                time = datetime.datetime(
                        year=int(parts[1]),
                        month=int(parts[2]),
                        day=int(parts[3]),
                        hour=int(parts[4]),
                        minute=int(parts[5]))
                if time < min_time:
                    print 'Removing %s' % filename
                    os.remove('%s/%s' % (PHOTOS_DIR, filename,))
            except:
                print '%s ommited' % filename
    print 'Done'


if __name__ == '__main__':
    remove_old_photos()
