#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys, os, daemon
from flup.server.fcgi import WSGIServer

from settings import PHOTOS_DIR


SOCKET = '/tmp/fastcgi.sock'


def app(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])

    yield '<h1>Photos</h1>'
    yield '<ul>'
    for root, dirnames, filenames in os.walk(PHOTOS_DIR):
        for filename in filenames:
            yield '<li><a href="/photos/{fn}" target="_blank">{fn}</a>'.format(fn=filename)
    yield '</ul>'


if __name__ == '__main__':
    """
    usage: ``python fastcgi.py`` to run server in console or
    ``python fastcgi.py daemonize`` to run server as daemon.
    """
    if 'daemonize' in sys.argv:
        with daemon.DaemonContext():
            WSGIServer(app, bindAddress=SOCKET).run()
    else:
        WSGIServer(app, bindAddress=SOCKET).run()
