Few scripts to make photo registrator from raspberry pi
=======================================================

Files
-----

- cameraman_server - init.d script for FastCGI server
- capture.sh - script to take a photo from webcam
- cleaner.py - script for removing all photos
- fastcgi.py - simple FastCGI server for showing available photos
- fastcgi_params - fascgi config for nginx
- nginx.conf - nginx configuration
- readme.rst - this file
- requirements.txt - necessary python packages list
- settings.py - some configurations

Dependencies
------------

- fswebcam
- git
- nginx
- python-virtuaenv

Installation
------------

::

    useradd --create-home cameraman

    sudo apt-get install nginx git python-virtualenv fswebcam
    git clone https://nanvel@bitbucket.org/nanvel/cameraman.git
    cd cameraman
    virtualenv .env --no-site-packages
    source .env/bin/activate
    pip install -r requirements

    sudo mv nginx.conf /etc/nginx/
    sudo mv cameraman_server /etc/init.d/
    sudo chmod +x /etc/init.d/cameraman_server
    sudo update-rc.d cameraman_server defaults

    sudo mkdir /home/cameraman/photos
    cd ..
    sudo mv cameraman /home/cameraman/

    sudo chown cameraman /home/cameraman/cameraman
    sudo chown cameraman /home/cameraman/photos

    sudo /etc/init.d/nginx restart
    sudo /etc/init.d/cameraman_server restart


Edit crontabs:

::

    *  *    * * *   root sh /home/cameraman/cameraman/capture.sh
    10 *    * * *   cameraman cd /home/cameraman/cameraman && .env/bin/activate && python cleaner.py

Links
-----

See more at http://nanvel.name
